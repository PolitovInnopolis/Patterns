package factorymethods;

import singleton.EnumSingleton.holdsingleton.Singleton;

public class CreatorTrain extends Creator{

    @Override
    public Transport create() {
        return new Train();
    }

    private CreatorTrain() {
    }

    public static class CreatorTrainHolder {
        private static CreatorTrain INSTANCE = new CreatorTrain();

        public static CreatorTrain getInstance(){
            return INSTANCE;
        }
    }
}
