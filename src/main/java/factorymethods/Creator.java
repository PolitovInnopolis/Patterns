package factorymethods;

/**
 * Created by General on 3/7/2017.
 */
public abstract class Creator {
    public abstract Transport create();
}
