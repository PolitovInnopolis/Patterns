package factorymethods;

public class CreatorCar extends Creator{

    @Override
    public Transport create() {
        return new Car();
    }

    private CreatorCar() {
    }

    public static class CreatorCarHolder {
        private static CreatorCar INSTANCE = new CreatorCar();

        public static CreatorCar getInstance(){
            return INSTANCE;
        }
    }
}

