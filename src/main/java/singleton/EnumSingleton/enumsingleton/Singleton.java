package singleton.EnumSingleton.enumsingleton;

/**
 * С помощью enum
 * Потокобезопасный
 */
public enum Singleton {
    INSTANCE;
    private Object obj = null;
    public Object createInstance(){
        if(obj == null){
            obj = new Object();
        }
        return obj;
    }
}
