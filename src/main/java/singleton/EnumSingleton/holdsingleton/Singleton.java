package singleton.EnumSingleton.holdsingleton;

/**
 * Created by General on 3/7/2017.
 */
public class Singleton {

    private Singleton(){}

    static class SingletonHolder {
        private static Singleton INSTANCE = new Singleton();

        public Singleton getInstance(){
            return INSTANCE;
        }
    }
}
