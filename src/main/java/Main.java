import factorymethods.Creator;
import factorymethods.CreatorTrain;
import structurepattens.decorator.decorator.Chees;
import structurepattens.decorator.decorator.Mushrooms;
import structurepattens.decorator.decorator.Pizza;
import structurepattens.decorator.decorator.Tomato;
import structurepattens.decorator.structurepatterns.adapter.AdapterCompozitionVk;
import structurepattens.decorator.structurepatterns.adapter.AdapterGeneralizeTelegram;
import structurepattens.decorator.structurepatterns.adapter.Bot;

import java.util.ArrayList;

/**
 * Created by General on 3/7/2017.
 */
public class Main {
    public static void main(String[] args) {



        Bot bot1 = new AdapterGeneralizeTelegram();
        Bot bot2 = new AdapterCompozitionVk();

        bot1.sendMessage("Message sent to Telegram", 1);
        bot1.sendSpam("Spam sent to Telegram", 5);
        bot1.sleep(1000f);

        bot2.sendMessage("Message sent to VK", 2);
        bot2.sendSpam("Spam sent to VK", 8);
        bot2.sleep(1000f);

        Pizza pizza = new Pizza();
        Chees chees = new Chees(new Tomato(new Chees(new Mushrooms(pizza))));
        chees.showComponent();



        ArrayList<Creator> creators = new ArrayList<Creator>();
        creators.add(CreatorTrain.CreatorTrainHolder.getInstance());
        creators.add(CreatorTrain.CreatorTrainHolder.getInstance());
    }
}
