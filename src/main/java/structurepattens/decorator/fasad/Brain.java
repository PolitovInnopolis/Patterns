package structurepattens.decorator.fasad;

/**
 * Created by General on 3/8/2017.
 */
public class Brain {
    public void think(String thought){
        System.out.println("I think of " + thought);
    }

    public void stayCold(int temp){
        System.out.println("Brain temp is " + temp);
    }
}
