package structurepattens.decorator.fasad;

import java.util.Hashtable;

/**
 * Created by General on 3/8/2017.
 */
public class Human {
    private Brain brain;
    private Heart heart;
    private Hands hands;

    public Human() {
        brain = new Brain();
        heart = new Heart();
        hands = new Hands();
    }

    public void life(){
        brain.stayCold(100);
        brain.think("8 marta");
        hands.doWork();
        heart.stateHot(120);
        heart.takeRitm();
    }
}
