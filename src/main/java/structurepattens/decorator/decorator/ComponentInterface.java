package structurepattens.decorator.decorator;

/**
 * Created by General on 3/8/2017.
 */
public interface ComponentInterface {
    void showComponent();
}
