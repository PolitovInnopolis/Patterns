package structurepattens.decorator.decorator;

/**
 * Created by General on 3/8/2017.
 */
    public class Mushrooms extends PizzaComponent {
    public Mushrooms(ComponentInterface component) {
        super(component);
    }

    @Override
    public void showComponent() {
        super.showComponent();
        System.out.println("Mushrooms");
    }
}
