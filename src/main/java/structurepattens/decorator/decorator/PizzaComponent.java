package structurepattens.decorator.decorator;

/**
 * Created by General on 3/8/2017.
 */
public class PizzaComponent implements ComponentInterface {
    private ComponentInterface component;

    public PizzaComponent(ComponentInterface component) {
        this.component = component;
    }

    public void showComponent() {
        component.showComponent();
    }
}
