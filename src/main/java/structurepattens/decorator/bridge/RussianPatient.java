package structurepattens.decorator.bridge;

/**
 * Created by General on 3/8/2017.
 */
public class RussianPatient extends Patient {
    protected RussianPatient(IHospital iHospital) {
        super(iHospital);
    }

    public void heal() {
        iHospital.heal();
    }
}
