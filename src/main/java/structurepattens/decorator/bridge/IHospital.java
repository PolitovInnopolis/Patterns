package structurepattens.decorator.bridge;

/**
 * Created by General on 3/8/2017.
 */
public interface IHospital {
    void heal();
}
