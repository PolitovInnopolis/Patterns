package structurepattens.decorator.bridge;

/**
 * Created by General on 3/8/2017.
 */
public abstract class Patient {
    protected IHospital iHospital;

    protected Patient(IHospital iHospital) {
        this.iHospital = iHospital;
    }

    public abstract void heal();
}
