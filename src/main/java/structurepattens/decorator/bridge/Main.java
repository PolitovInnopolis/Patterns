package structurepattens.decorator.bridge;

/**
 * Created by General on 3/8/2017.
 */
public class Main {
    public static void main(String[] args) {
        Patient kyleRease = new RussianPatient(new Surgery());
        Patient sarahConnor = new RussianPatient(new Ginekology());
        Patient[] patients = {kyleRease, sarahConnor};

        for (Patient patient :
                patients) {
            patient.heal();
        }
    }
}
