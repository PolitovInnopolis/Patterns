package structurepattens.decorator.structurepatterns.adapter;

/**
 * Created by General on 3/8/2017.
 */
public class AdapterCompozitionVk implements Bot {
    private VkBot bot = new VkBot();

    private Boolean checkIsFriend(){
        return false;
    }
    private Integer getDelay(){
        return 100;
    }

    public void sendMessage(String message, Integer userId) {
        bot.sendMessage(message, checkIsFriend(), userId);
    }

    public void sendSpam(String spam, Integer ppl) {
        bot.sendSpam(ppl,spam, getDelay());
    }

    public void sleep(Float millis) {
        bot.sleep();
    }
}
