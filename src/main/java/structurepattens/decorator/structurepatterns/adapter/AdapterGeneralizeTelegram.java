package structurepattens.decorator.structurepatterns.adapter;

/**
 * Created by General on 3/8/2017.
 */
public class AdapterGeneralizeTelegram extends TelegemBot
                                        implements Bot{
    private Integer searchGroupId(){
        return 1;
    }

    public void sendMessage(String message, Integer userId) {
        this.sendMessage(message, searchGroupId(), userId);
    }

    public void sendSpam(String spam, Integer ppl) {
        this.sendSpam(ppl, spam);
    }

    public void sleep(Float millis) {
        this.sleep(Integer.valueOf(millis.toString()));
    }
}
