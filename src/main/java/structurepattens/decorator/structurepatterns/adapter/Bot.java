package structurepattens.decorator.structurepatterns.adapter;

/**
 * Created by General on 3/8/2017.
 */
public interface Bot {
    void sendMessage(String message, Integer userId);
    void sendSpam(String spam, Integer ppl);
    void sleep (Float millis);
}
