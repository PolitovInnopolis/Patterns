package abstractfactory.ikea;

import abstractfactory.*;

/**
 * Created by General on 3/7/2017.
 */
public class FactoryIkea extends MebelFactory {
    public Chair createChair() {
        return new ChairIkea();
    }

    public Table createTable() {
        return new TableIkea();
    }

    @Override
    public Taburetka createTaburetka() {
        return new TaburetkaIkea();
    }

    public Sofa createSofa() {
        return new SofaIkea();
    }
}
