package abstractfactory;

/**
 * Created by General on 3/7/2017.
 */
public abstract class MebelFactory {
    public abstract Chair createChair();
    public abstract Table createTable();
    public abstract Sofa createSofa();
    public abstract Taburetka createTaburetka();
}
