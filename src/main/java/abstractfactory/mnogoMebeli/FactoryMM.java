package abstractfactory.mnogoMebeli;

import abstractfactory.*;

/**
 * Created by General on 3/7/2017.
 */
public class FactoryMM extends MebelFactory {
    public Chair createChair() {
        return new ChairMM();
    }

    public Table createTable() {
        return new TableMM();
    }

    public Sofa createSofa() {
        return new SofaMM();
    }

    @Override
    public Taburetka createTaburetka() {
        return new TaburetkaMM();
    }
}
