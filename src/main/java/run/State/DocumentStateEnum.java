package run.State;

/**
 * Created by General on 3/9/2017.
 */
public enum DocumentStateEnum {
    LOADED,
    PREPARED,
    SIGNED_1,
    SEND,
    RECEIVED,
    SIGNED_2,
    ACCEPT,
    SEND_APPRUVMENT,
    RECEIVE_APPRUVMENT,

    ERROR_SIGNED,
    REJECTED
}
