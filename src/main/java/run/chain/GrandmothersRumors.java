package run.chain;

/**
 * Created by General on 3/9/2017.
 */
public class GrandmothersRumors extends Rumors {

    @Override
    public void writeRumors(String message) {
        System.out.println("GrandmothersRumors say " + message);
    }
}
