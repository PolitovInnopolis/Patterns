package run.chain;

/**
 * Created by General on 3/9/2017.
 */
public abstract  class Rumors {


    protected Rumors rumors;

    boolean isTrue;

    public Rumors setNext(Rumors rumors) {
        this.rumors = rumors;
        return this.rumors;
    }

    public abstract void writeRumors(String message);

    public void chain(String message){
        if(isTrue){
            writeRumors(message);
            return;
        }
        if(this.rumors != null){
            this.rumors.chain(message);
        }
    }
}
