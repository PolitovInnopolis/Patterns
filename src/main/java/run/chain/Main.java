package run.chain;

/**
 * Created by General on 3/9/2017.
 */
public class Main {
    public static void main(String[] args) {
        OfficeRumors officeRumors = new OfficeRumors();
        TeshaRumors teshaRumors = new TeshaRumors();
        GrandmothersRumors grandmothersRumors = new GrandmothersRumors();
        officeRumors.isTrue = false;
        teshaRumors.isTrue = true;
        grandmothersRumors.isTrue = false;
        officeRumors.setNext(teshaRumors.setNext(grandmothersRumors));

        officeRumors.chain(" rumors about ....");
    }
}
