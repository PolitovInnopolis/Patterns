package run.strategy;

/**
 * Created by General on 3/9/2017.
 */
public class BankStrategy extends SendStrategy {

    public BankStrategy() {

    }

    public void sendMoney(int money) {
        System.out.println("You send " + money + " with percent "+ getPercent() + "% at time " +
                getTime());
    }



}
