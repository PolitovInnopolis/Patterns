package run.strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by General on 3/9/2017.
 */
public class Context {
    private SendStrategy currentStrategy;
    private List<SendStrategy> aviableStrategy = new ArrayList<SendStrategy>();

    public void sendMoney(int summ){
        findStrategegy();
        currentStrategy.sendMoney(summ);
    }

    private void findStrategegy(){
        SendStrategy tempStrategy = new BankStrategy();
        tempStrategy.setPercent(100);
        tempStrategy.setTime(200);
        for (SendStrategy strategy :
                aviableStrategy) {
            if (strategy.getTime() > 4) {
                continue;
            }

            if (strategy.getPercent()< tempStrategy.getPercent()){
                tempStrategy = strategy;
            }
        }
        currentStrategy = tempStrategy;
    }

    public void add(SendStrategy sendStrategy){
        aviableStrategy.add(sendStrategy);
    }
}
