package run.strategy;

/**
 * Created by General on 3/9/2017.
 */
public class MailStrategy extends SendStrategy {

    public MailStrategy() {
        setTime(3);
    }

    public void sendMoney(int money) {
        System.out.println("You send " + money + " with percent "+ getPercent() + "% at delay three day");
    }


}
