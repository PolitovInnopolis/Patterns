package run.strategy;

/**
 * Created by General on 3/9/2017.
 */
public abstract class SendStrategy {
    private int percent;
    private int time;

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public abstract void  sendMoney(int money);

}
