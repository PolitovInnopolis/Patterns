package run.strategy;

/**
 * Created by General on 3/9/2017.
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context();

        SendStrategy bankStrategy = new  BankStrategy();
        bankStrategy.setPercent(10);
        bankStrategy.setTime(1);
        SendStrategy mailStrategy = new MailStrategy();
        SendStrategy webMoneyStrategy = new WebMoneyStartegy();
        webMoneyStrategy.setPercent(20);

        context.add(bankStrategy);
        context.add(mailStrategy);
        context.add(webMoneyStrategy);

        context.sendMoney(100);


    }
}
