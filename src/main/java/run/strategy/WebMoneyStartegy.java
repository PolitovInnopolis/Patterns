package run.strategy;

/**
 * Created by General on 3/9/2017.
 */
public class WebMoneyStartegy extends SendStrategy {
    public WebMoneyStartegy() {
        setPercent(200);
    }

    public void sendMoney(int money) {
        System.out.println("You send " + money + " with fix commision = 200RUR");
    }

}
