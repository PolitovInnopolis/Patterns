package run.observer;

/**
 * Created by General on 3/9/2017.
 */
public interface IObserver {
    void message(String mess);
}
