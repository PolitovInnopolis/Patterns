package run.observer;

/**
 * Created by General on 3/9/2017.
 */
public class Main {
    public static void main(String[] args) {
        HR hr = new HR(1, "Masha");
        HR hr1 = new HR(2, "Ola");


        Searcher searcher1 = new Searcher(1);
        Searcher searcher2 = new Searcher(2);
        Searcher searcher3 = new Searcher(3);
        Searcher searcher4 = new Searcher(4);
        Searcher searcher5 = new Searcher(5);

        hr.addObserver(searcher1);
        hr.addObserver(searcher2);
        hr.addObserver(searcher3);

        hr1.addObserver(searcher3);
        hr1.addObserver(searcher4);
        hr1.addObserver(searcher5);

        hr.notifyAllObserver();
        hr1.notifyAllObserver();
    }
}
