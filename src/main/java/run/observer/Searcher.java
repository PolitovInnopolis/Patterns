package run.observer;

/**
 * Created by General on 3/9/2017.
 */
public class Searcher implements  IObserver{
    private int id;

    public Searcher(int id) {
        this.id = id;
    }

    public void message(String mess) {
        System.out.println("New message for " + id + mess);
    }
}
