package run.observer;

/**
 * Created by General on 3/9/2017.
 */
public interface IObservable {
    void addObserver(IObserver observer);
    void removeObserver(IObserver observer);
    void  notifyAllObserver();

}
