package run.observer;

import java.util.ArrayList;

/**
 * Created by General on 3/9/2017.
 */
public class HR implements IObservable {
    private ArrayList<IObserver> observers = new ArrayList<IObserver>();
    private int id;
    private String name;

    public HR(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addObserver(IObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(IObserver observer) {
        observers.remove(observer);
    }

    public void notifyAllObserver() {
        for (IObserver observer :
                observers) {
            observer.message("I am update");
        }
    }
}
