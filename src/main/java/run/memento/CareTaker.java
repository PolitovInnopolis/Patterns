package run.memento;

/**
 * Created by General on 3/9/2017.
 */
public class CareTaker {
    private  Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
