package run.memento;

/**
 * Created by General on 3/9/2017.
 */
public class Level {
    private String state;

    public Memento saveState(){
        return new Memento(state);
    }

    public void restoreState(Memento memento){
        this.state = memento.getState();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
