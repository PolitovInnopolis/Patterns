package run.memento;

/**
 * Created by General on 3/9/2017.
 */
public class Main {
    public static void main(String[] args) {
        Level level = new Level();
        level.setState("level1");
        CareTaker taker = new CareTaker();
        taker.setMemento(level.saveState());
        level.setState("level2");
        System.out.println(level.getState());
        level.restoreState(taker.getMemento());
        System.out.println(level.getState());

    }
}
