package builder;

/**
 * Created by General on 3/7/2017.
 */
public abstract  class WindowBuilder {
    protected AuWindow  auWindow;



    public void create(){
        auWindow = new AuWindow();

    }
    public AuWindow getAuWindow() {
        if (auWindow != null) {
            return auWindow;
        }
        else  return new AuWindow();
    }
    public abstract void buildModel();
    public abstract void buildHeight();
    public abstract void buildWidht();

}
