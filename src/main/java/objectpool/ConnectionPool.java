package objectpool;

/**
 * Created by General on 3/7/2017.
 */
public class ConnectionPool extends ObjectPool<Connection> {

    private ConnectionPool(){
        super();
    }

    ObjectPool getInstance() {
        return SingletonHolder.getInstance();
    }

    static class SingletonHolder {
        private static ConnectionPool INSTANCE = new ConnectionPool();

        public static ConnectionPool getInstance(){
            return INSTANCE;
        }
    }


    Connection create() {
        return new Connection();
    }

    boolean validate(Connection o) {
        return false;
    }


}
