package objectpool;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * Created by General on 3/7/2017.
 */
public abstract class ObjectPool<T> {
    private Long expirationTime;
    private Hashtable unlocked;
    private Long maxSize;
    private Hashtable locked;

    public ObjectPool() {
        locked = new Hashtable();
        unlocked = new Hashtable();
        expirationTime = 30000L;
        maxSize = 16L;
    }

    abstract T create();
    abstract boolean validate( T o );
    void expire( T o ){};
    synchronized T checkOut()
    {
        long now = System.currentTimeMillis();
        T o;
        if( unlocked.size() > 0 ) {
            Enumeration e = unlocked.keys();
            while( e.hasMoreElements() ){
                o =(T)e.nextElement();
                if( ( now - ( ( Long ) unlocked.get( o ) ).longValue() ) >
                        expirationTime ){
                    // object has expired
                    unlocked.remove( o );
                    expire( o );
                    o = null;
                }else{
                    if( validate( o ) ){
                        unlocked.remove( o );
                        locked.put( o, new Long( now ) );
                        return( o );
                    }else{
                        // object failed validation
                        unlocked.remove( o );
                        expire( o );
                        o = null;
                    }
                }
            }
        }
        // no objects available, create a new one
        o = create();
        locked.put( o, new Long( now ) );
        return( o );
    }
    synchronized void checkIn( T o ){
        locked.remove( o );
        unlocked.put( o, new Long( System.currentTimeMillis() ) );
    }

    abstract ObjectPool getInstance();
}
