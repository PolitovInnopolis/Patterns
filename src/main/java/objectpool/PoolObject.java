package objectpool;

/**
 * Created by General on 3/7/2017.
 */
public class PoolObject {
    private Boolean busy;

    public Boolean getBusy() {
        return busy;
    }

    public void setBusy(Boolean busy) {
        this.busy = busy;
    }
}
